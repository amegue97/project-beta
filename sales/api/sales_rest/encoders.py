from common.json import ModelEncoder
from sales_rest.models import SalesPerson, Sale, Customer, AutomobileVO

class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["id", "name", "employee_id"]


class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = ["id", "name", "address", "phone"]


class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = ["id", "name"]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["id", "import_href", "import_vin"]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = ["id", "price", "sales_person", "customer", "automobile"]
    encoders = {
        "sales_person": SalesPersonEncoder(),
        "customer": CustomerDetailEncoder(),
        "automobile": AutomobileVOEncoder(),
    }    


   
    