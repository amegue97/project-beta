from django.urls import path
from .views import (
    api_sales_people,
    api_sales_person,
    api_customers,
    api_customer,
    api_sales,
    api_sale,
    delete_automobile_vo,
    api_available_automobiles
)

urlpatterns =[
    path("sales-people/", api_sales_people, name="api_sales_people",),
    path("sales-people/<int:employee_id>/", api_sales_person, name="api_sales_person",),
    path("customers/", api_customers, name="api_customers",),
    path("customers/<int:pk>/", api_customer, name="api_customer",),
    path("sales/", api_sales, name="api_sales",),
    path("sales/<int:pk>/", api_sale, name="api_sale",),
    path("sales-people/<int:employee_id>/sales/", api_sales, name="api_sales_person_sales",),
    path("automobilesvo/<str:vin>/", delete_automobile_vo, name="api_delete_automobile_vo"),
    path("available-autos/", api_available_automobiles, name="api_available_autos")
]