from django.shortcuts import render
from .models import SalesPerson, Sale, AutomobileVO, Customer
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from django.core import serializers
from .encoders import (
    SalesPersonEncoder, 
    SaleEncoder, 
    CustomerListEncoder, 
    CustomerDetailEncoder, 
    AutomobileVOEncoder)


@require_http_methods(["GET", "POST"])
def api_sales_people(request):
    if request.method == "GET":
        sales_people = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_people": sales_people},
            encoder=SalesPersonEncoder,
            safe = False,
        )
    else:
        content = json.loads(request.body)

        sales_person = SalesPerson.objects.create(**content)
        return JsonResponse(
            sales_person,
            encoder = SalesPersonEncoder,
            safe = False,
        )    


@require_http_methods(["GET", "PUT", "DELETE"])
def api_sales_person(request, employee_id):
    if request.method == "GET":
        sales_person = SalesPerson.objects.get(employee_id=employee_id)
        return JsonResponse(
            {"sales_person": sales_person}, 
            encoder = SalesPersonEncoder,
            safe = False,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        SalesPerson.objects.filter(employee_id = employee_id).update(**content)
        sales_person = SalesPerson.objects.get(employee_id = employee_id)
        return JsonResponse(
            sales_person, 
            encoder = SalesPersonEncoder,
            safe = False,
        )  
    else:
        count, _ = SalesPerson.objects.get(employee_id = employee_id).delete()
        return JsonResponse({"deleted": count > 0})      



@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder = CustomerListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer, 
            encoder = CustomerDetailEncoder,
            safe = False,
        )    


@require_http_methods(["GET", "PUT", "DELETE"])
def api_customer(request, pk):
    if request.method == "GET":
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            {"customer": customer}, 
            encoder = CustomerDetailEncoder,
            safe = False,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        Customer.objects.filter(id = pk).update(**content)
        customer = Customer.objects.get(id = pk)
        return JsonResponse(
            customer, 
            encoder = CustomerDetailEncoder,
            safe = False,
        )  
    else:
        count, _ = Customer.objects.get(id = pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_sales(request, employee_id=None):
    if request.method == "GET":
        if employee_id != None:
            sales_person = SalesPerson.objects.get(employee_id = employee_id)
            sales = Sale.objects.filter(sales_person = sales_person)
        else:
            sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder = SaleEncoder,
            safe=False,
        )        

    else:
        content = json.loads(request.body)

        sales_person = SalesPerson.objects.get(employee_id = content["sales_person"])
        content["sales_person"] = sales_person
        

        automobile = AutomobileVO.objects.get(import_vin = content["automobile_vin"])
        content["automobile"] = automobile
        del content["automobile_vin"]

        customer = Customer.objects.get(id = content["customer_id"])
        content["customer"] = customer 
        del content["customer_id"]

        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale, 
            encoder = SaleEncoder,
            safe=False,
        )
           


@require_http_methods(["GET", "PUT", "DELETE"])
def api_sale(request, pk):
    if request.method == "GET":
        sale = Sale.objects.get(id=pk)
        return JsonResponse(
            {"sale": sale}, 
            encoder = SaleEncoder,
            safe = False,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        Sale.objects.filter(id = pk).update(**content)
        sale = Sale.objects.get(id = pk)
        return JsonResponse(
            sale, 
            encoder = SaleEncoder,
            safe = False,
        )  
    else:
        count, _ = Sale.objects.get(id = pk).delete()
        return JsonResponse({"deleted": count > 0})



def delete_automobile_vo(request, vin):
    if request.method == "DELETE":
        count, _ = AutomobileVO.objects.get(import_vin = vin).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )


def api_available_automobiles(request):
    if request.method == "GET":
        automobiles = serializers.serialize("python", AutomobileVO.objects.all())
        sales = serializers.serialize("python", Sale.objects.all())
        available_autos = []

        for auto in automobiles:
            sold = False
            for sale in sales:
                if auto["pk"] == sale["fields"]["automobile"]: 
                    sold = True
                    break
            if sold == False:
                available_autos.append(AutomobileVO.objects.get(id=auto["pk"]))

        response = []
        vehicles = AutomobileVO.objects.all()
        for vehicle in vehicles:
            if vehicle in available_autos:
                response.append(
                    {
                        "import_href": vehicle.import_href,
                        "import_vin": vehicle.import_vin,
                    }
                )    

    return JsonResponse(
        {"available_autos": response},
    )