from django.db import models
from django.urls import reverse

class SalesPerson(models.Model):
    name = models.CharField(max_length=100)
    employee_id = models.PositiveBigIntegerField()

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_sales_person", kwargs={"pk": self.id})


class Customer(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=200)
    phone = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_customer", kwargs={"pk": self.id})


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, null=True)
    import_vin = models.CharField(max_length=17, unique=True, null=True)

    def __str__(self):
        return self.import_vin


class Sale(models.Model):
    sales_person = models.ForeignKey('SalesPerson', related_name="sales", on_delete=models.PROTECT)
    automobile = models.OneToOneField('AutomobileVO', on_delete=models.PROTECT)
    customer = models.ForeignKey('Customer', related_name="sales", on_delete=models.PROTECT)
    price = models.CharField(max_length=200)

    def get_api_url(self):
        return reverse("api_sale", kwargs={"pk": self.id})
    