from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .encoders import AppointmentListEncoder, TechnicianDetailEncoder, TechnicianListEncoder

from .models import AutomobileVO, ServiceAppointment, Technician

# @require_http_methods(["GET"])
# def api_detail_service_history(request):
#     pass

@require_http_methods(["GET", "POST"])
def api_service_appointment(request):
    if request.method == "GET":
        try:
            service_appointments = ServiceAppointment.objects.all()
            return JsonResponse(
            # {"sevice_appointments" : service_appointments},
                service_appointments,
                encoder=AppointmentListEncoder, safe=False
        )
        except ServiceAppointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    else: #POST
        content = json.loads(request.body)
        technician = Technician.objects.get(employee_number=content["tech_name"])
        content["tech_name"] = technician

        try:
            vin = AutomobileVO.objects.get(vin=content["vin"])
            content["vip"] = True

        except AutomobileVO.DoesNotExist:
            content["vip"]= False
        service_appointment = ServiceAppointment.objects.create(**content)

        return JsonResponse(
            service_appointment, encoder=AppointmentListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_service_appointments(request, pk):
    if request.method == "GET":
        try:
            service_appointment = ServiceAppointment.objects.get(id=pk)
            return JsonResponse(
                service_appointment,
                encoder=AppointmentListEncoder,
                safe=False
            )
        except ServiceAppointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            service_appointment = ServiceAppointment.objects.get(id=pk)
            service_appointment.delete()
            return JsonResponse(
                service_appointment,
                encoder=AppointmentListEncoder,
                safe=False,
            )
        except ServiceAppointment.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            service_appointment = ServiceAppointment.objects.get(id=pk)

            props = ["tech_name", "appointment_date", "appointment_time"]
            for prop in props:
                if prop in content:
                    setattr(service_appointment, prop, content[prop])
            service_appointment.save()
            return JsonResponse(
                service_appointment,
                encoder=AppointmentListEncoder,
                safe=False,
            )
        except service_appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_technician(request):
    if request.method == "GET":
        technician = Technician.objects.all()
        return JsonResponse(
            technician, encoder = TechnicianListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianDetailEncoder,
            safe=False,
        )
