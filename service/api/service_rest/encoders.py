from .models import Technician, ServiceAppointment, AutomobileVO
from common.json import ModelEncoder
from django.http import JsonResponse
import json


# class AppointmentDetailEncoder(ModelEncoder):
#     pass

# class AutomomobileVOListEncoder(ModelEncoder):
#     pass


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["import_href",'vin']

class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = ["tech_name", "employee_number"]


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = ["tech_name", "employee_number"]


class AppointmentListEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "customer_name",
        "tech_name",
        "appointment_date",
        "appointment_time",
        "service_reason",
        "vin",
        "vip",
    ]

    encoders = {
        "tech_name" : TechnicianListEncoder()
    }
