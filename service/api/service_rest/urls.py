from django.urls import path
from .views import api_technician, api_service_appointments, api_service_appointment


urlpatterns = [

    # GET Request - LIST OF VIN SERVICE HISTORY - Detail View
    # path("automobiles/<automobile_VO_id>/", api_detail_service_history, name="api_VIN_service_details"),


    path("service_appointments/<int:pk>/", api_service_appointments, name="api_list_service_appointments"),

    path("service_appointment/", api_service_appointment, name="api_create_service_appointment"),

    path("technicians/", api_technician,
    name="api_create_technician"),

    # path("technicians/<int:pk>", api_technicians, "api_view_technician"),

]