from django.db import models


class Technician(models.Model):
    tech_name = models.CharField(max_length=100, null = True)
    employee_number = models.PositiveSmallIntegerField(unique= True, null = True)

    def __str__(self):
        return self.tech_name

class ServiceAppointment(models.Model):
    customer_name = models.CharField(max_length=100)
    tech_name = models.ForeignKey("Technician", related_name="appointments",on_delete=models.PROTECT)
    appointment_date = models.DateField(null = True)
    appointment_time = models.TimeField(null = True)
    service_reason = models.CharField(max_length=500)
    vin=models.CharField(max_length = 17, unique = True, null = True)
    vip = models.BooleanField(null = True, default = False)

    # def __str__(self):
    #     return self.name

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=100, null = True, unique=True)

    def __str__(self):
        return self.vin
