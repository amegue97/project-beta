import { useEffect, useState } from "react";

function AppointmentList() {
    const [serviceAppointments, setServiceAppointments] = useState(null);

    useEffect(() => {
        async function getServiceAppointments() {
            const response = await fetch('http://localhost:8080/api/service_appointment/');
            const data = await response.json();
            setServiceAppointments(data);
        }
        if (serviceAppointments === null) {
            getServiceAppointments()
        }
    }, [])

    if (serviceAppointments === null) {
        return(
            <p>Loading...</p>
        )
    }
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Customer Name</th>
                    <th>Date of Appointment</th>
                    <th>Time of Appointment</th>
                    <th>Assigned Technician</th>
                    <th>Service Reason</th>
                </tr>
            </thead>
            <tbody>
                {serviceAppointments.map(service_appointment => {
                    return (
                        <tr key={service_appointment.vin }>
                            <td>{ service_appointment.vin } </td>
                            <td>{ service_appointment.customer_name } </td>
                            <td>{ service_appointment.appointment_date } </td>
                            <td>{ service_appointment.appointment_time } </td>
                            <td>{ service_appointment.tech_name.tech_name } </td>
                            <td>{ service_appointment.service_reason } </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default AppointmentList;
