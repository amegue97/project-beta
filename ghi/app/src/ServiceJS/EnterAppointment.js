import React from 'react';

class AppointmentForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            customer_name: '',
            tech_name: '',
            appointment_date: '',
            appointment_time: '',
            service_reason: '',
            vin: '',
            vip: '',
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeCustomerName = this.handleChangeCustomerName.bind(this);
        this.handleChangeTechName = this.handleChangeTechName.bind(this);
        this.handleChangeDate = this.handleChangeDate.bind(this);
        this.handleChangeTime = this.handleChangeTime.bind(this);
        this.handleChangeService = this.handleChangeService.bind(this);
        this.handleChangeVIN = this.handleChangeVIN.bind(this);
        this.handleChangeVIP = this.handleChangeVIP.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.State};

        const serviceURL = 'http://localhost:3000/service_appointment/new/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
      },
    };
        const response = await fetch(serviceURL, fetchConfig);
        if (response.ok) {
          const newAppointment = await response.json();
          console.log(newAppointment);
          this.setState({
                customer_name: '',
                tech_name: '',
                appointment_date: '',
                appointment_time: '',
                service_reason: '',
                vin: '',
                vip: '',
      });
    }
}

    handleChangeCustomerName(event) {
        const value = event.target.value;
        this.setState({ customer_name: value});
    }

    handleChangeTechName(event) {
        const value = event.target.value;
        this.setState({ tech_name: value});
    }

    handleChangeDate(event) {
        const value = event.target.value;
        this.setState({ appointment_date: value});
    }

    handleChangeTime(event) {
        const value = event.target.value;
        this.setState({ appointment_time: value});
    }

    handleChangeService(event) {
        const value = event.target.value;
        this.setState({ service_reason: value});
    }

    handleChangeCustomerName(event) {
        const value = event.target.value;
        this.setState({ customer_name: value});
    }

    handleChangeVIN(event) {
        const value = event.target.value;
        this.setState({ vin: value});
    }

    handleChangeVIP(event) {
        const value = event.target.value;
        this.setState({ vip: value});
    }

render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Enter a Service Appointment</h1>
            {/* <image  */}
            <form onSubmit={this.handleSubmit} id="create-conference-form">

              <div className="form-floating mb-3">
                <input onChange={this.handleChangeName} placeholder="Customer Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Customer Name</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={this.handleChangeTechName} placeholder="Tech Name" required type="text" name="tech name" id="tech name" className="form-control" />
                <label htmlFor="tech_name">Tech Name</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={this.handleChangeDate} placeholder="Ends" required type="date" name="appointment date" id="appointment date" className="form-control" />
                <label htmlFor="appointment_date">Appointment Date</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={this.handleChangeTime} placeholder="Appointment Time" required type="text" id="appointment time" name="appointment time" className="form-control" />
                <label htmlFor="appointment_time">Appointment Time</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={this.handleChangeService} placeholder="Service Reason" required type="text" name="Service Reason" id="Service Reason" className="form-control" />
                <label htmlFor="service_reason">Service Reason</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={this.handleChangeVIN} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
                <label htmlFor="vin">VIN</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={this.handleChangeVIP} placeholder="VIP" required type="text" name="vip" id="vip" className="form-control" />
                <label htmlFor="vip">VIP?</label>
              </div>

              <div className="mb-3">
                <select onChange={this.handleChangeLocation} required name="location" id="location" className="form-select">

                  {/* <option value="">Choose a location</option>
                  {this.state.locations.map(location => {
                    return (
                      <option key={location.id} value={location.id}>{location.name}</option>
                    )
                  })} */}

                </select>
              </div>
              <button className="btn btn-success">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default AppointmentForm;