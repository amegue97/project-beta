import React from 'react';

class TechnicianForm extends React.Component {
    constructor(props) {
        super (props);
        this.state = {
            tech_name: '',
            employee_number: '',
        }
        this.handleTechNameChange = this.handleTechNameChange.bind(this);
        this.handleEmployeeNumberChange = this.handleEmployeeNumberChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    async handleSubmit(event) {
        event.PreventDefault()
        const data = {...this.state};

        const technicianURL = 'http://localhost:3000/technicians/new/'
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-type': 'application/json',
            }
        };
        const response = await fetch(technicianURL, fetchConfig)
        if (response.ok) {
            // const Technician = await response.json()

            const clearForm = {
                tech_name: '',
                employee_number: '',
            };
            this.setState(clearForm);
            }
        }

    handleTechNameChange = (event) => {
            event.preventDefault();
            this.setState({
                ...this.state,
                [event.target.tech_name]: event.target.value
            });
        }

    handleEmployeeNumberChange = (event) => {
        event.preventDefault();
        this.setState({
            ...this.setState,
            [event.target.employee_number]: event.target.value
            });
         }

        // const value = event.target.value;
        // this.setState({ employee_number : value});

    render() {
        return (
            <div className="row">
                <div className="offset-2 col-7">
                    <div className="shadow p-4 mt-4">
                        <h4>Create A New Service Technician</h4>
                        <form onSubmit={this.handleSubmit} id="create-service-technician">

                            <div className="form-floating mb-4">
                                <input onChange={this.handleTechNameChange} value={this.state.technician} placeholder="Technician Name" required type="text" name="technician name" className="form-control" />
                                <label htmlFor="technician_name">Technician Name</label>
                            </div>

                            <div className="form-floating mb-4">
                                <input onChange={this.handleEmployeeNumberChange} value={this.state.employee_number} placeholder="Employee Number" required type="number" name="employee number" className="form-control" />
                                <label htmlFor="Employee Number">Employee Number</label>
                            </div>

                        <button className="btn btn-success">Create</button>
                    </form>
                </div>
            </div>
        </div>
        );
    }
}
export default TechnicianForm