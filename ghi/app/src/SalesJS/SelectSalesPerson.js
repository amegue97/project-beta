import React from 'react';
import SalesPersonSales from './SalesPersonSales';

export default class SelectSalesPerson extends React.Component {
    constructor() {
        super();
        this.state = {
            salesPerson: '',
            salesPersonSales: [],
            salesPeople: []
        }
        this.handlePersonChange = this.handlePersonChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handlePersonChange(event) {
        this.setState(
            {salesPerson: event.target.value}
        )
    }

    async handleSubmit(event) {
        event.preventDefault();    

        const response = await fetch(`http://localhost:8090/api/sales-people/${this.state.salesPerson}/sales/`)
        if (response.ok) {
            const data = await response.json()
            this.setState(
                {salesPersonSales: data.sales}
            )
        }    
    }  

    async componentDidMount() {
        const response = await fetch('http://localhost:8090/api/sales-people/');
        
        if (response.ok){
            const data = await response.json();
            this.setState(
                {salesPeople: data.sales_people}
            );
        }
    }
    
    render() {
        return(
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Sales Person History</h1>
                        <form onSubmit={this.handleSubmit}>
                            <div className="mb-3">
                                <select onChange={this.handlePersonChange} id="select-sales-person" required name="salesPerson" className="form-select">
                                    <option value="default">Choose sales person</option>
                                    {this.state.salesPeople.map(salesPerson => {
                                        return(
                                            <option key={salesPerson.employee_id} value={salesPerson.employee_id}>
                                                {salesPerson.name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-success">See Sales</button>
                        </form>
                        <div>
                            <SalesPersonSales sales={this.state.salesPersonSales} />
                        </div>
                    </div>
                </div>
            </div>        
        )
    }
}
