export default function SalesPersonSales(props) {
    return(
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Sales Person</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Sale Price</th>
                </tr>
            </thead>
            <tbody>
                {props.sales.map(sale => {
                    return(
                        <tr key={sale.automobile.import_vin}>
                            <td>{sale.sales_person.name}</td>
                            <td>{sale.customer.name}</td>
                            <td>{sale.automobile.import_vin}</td>
                            <td>{sale.price}</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )
}