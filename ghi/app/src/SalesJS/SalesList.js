import React, { useState, useEffect } from 'react';

export default function SalesList() {
    const [sales, setSales] = useState(null);
    
    useEffect(() => {
        async function getSales () {
            const response = await fetch('http://localhost:8090/api/sales/');
            const data = await response.json();
            setSales(data.sales);
        };
        if (!sales) {
            getSales();
        }
    }, []);
    
    if (sales === null) {
        return (
            <>Loading...</>
        )
    }
    return (
        <div>
            <h1>Sales</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Sales Person</th>
                        <th>Employee Number</th>
                        <th>Customer</th>
                        <th>Automobile</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(sale => {
                        return(
                        <tr key={sale.automobile.import_href}>
                            <td>{sale.sales_person.name}</td>
                            <td>{sale.sales_person.employee_id}</td>
                            <td>{sale.customer.name}</td>
                            <td>{sale.automobile.import_vin}</td>
                            <td>{sale.price}</td>
                        </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    ) 
}

