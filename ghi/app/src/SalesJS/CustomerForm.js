import React from "react";

export default class CustomerForm extends React.Component {
    constructor() {
        super();
        this.state = {
            name: '',
            address: '',
            phone: ''

        }
        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();

        const data = {...this.state};
        const customerUrl = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
            const clearForm = {
                name: '',
                address: '',
                phone: ''
            }
            this.setState(clearForm);
        }
    }

    handleFieldChange(event) {
        this.setState({
            ...this.state,
            [event.target.name]: event.target.value
        })
    }

    render() {
        return(
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create New Customer</h1>
                        <form onSubmit={this.handleSubmit}>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleFieldChange} value={this.state.name} placeholder="Name" required type="text" name="name" className="form-control"/>
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleFieldChange} value={this.state.address} placeholder="Address" required type="text" name="address" className="form-control"/>
                                <label htmlFor="address">Address</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleFieldChange} value={this.state.phone} placeholder="Phone" required type="text" name="phone" className="form-control"/>
                                <label htmlFor="phone">Phone</label>
                            </div>
                            <div>
                                <button className="btn btn-success">Create</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>        
        )
    }
}