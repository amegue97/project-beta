import React from 'react';

export default class SalesPersonForm extends React.Component {
    constructor() {
        super();
        this.state = {
            name: '',
            employeeId: ''
        }
        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();

        const data = {};
        data["name"] = this.state.name;
        data["employee_id"] = this.state.employeeId;
        const salesPersonUrl = 'http://localhost:8090/api/sales-people/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(salesPersonUrl, fetchConfig);
        if (response.ok) {
            const clearForm = {
                name: '',
                employeeId: ''
            }
            this.setState(clearForm);
        }
    }

    handleFieldChange(event) {
        this.setState({
            ...this.state,
            [event.target.name]: event.target.value
        })
    }

    render() {
        return(
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create New Sales Person</h1>
                        <form onSubmit={this.handleSubmit}>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleFieldChange} value={this.state.name} placeholder="Name" required type="text" name="name" className="form-control"/>
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleFieldChange} value={this.state.employeeId} placeholder="Employee Id" required type="text" name="employeeId" className="form-control"/>
                                <label htmlFor="employeeId">Employee ID</label>
                            </div>
                            <button className="btn btn-success">Create</button>
                        </form>
                    </div>
                </div>
            </div>        
        )
    }
}