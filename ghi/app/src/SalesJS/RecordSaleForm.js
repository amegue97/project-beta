import React from "react"

export default class RecordSaleForm extends React.Component {
    constructor() {
        super();
        this.state= {
            automobile: '',
            automobiles: [],
            salesPerson: '',
            salesPeople: [],
            customer: '',
            customers: [],
            price: ''
        }
        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleFieldChange(event) {
        this.setState({
            ...this.state,
            [event.target.name]: event.target.value
        })
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {}
        data['sales_person'] = this.state.salesPerson;
        data['automobile_vin'] = this.state.automobile;
        data['customer_id'] = this.state.customer;
        data['price'] = this.state.price;

        const salesUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(salesUrl, fetchConfig)
        if(response.ok) {
            const clearForm = {
                automobile: '',
                salesPerson: '',
                customer: '',
                price: ''
            }
            this.setState(clearForm);
        }

    }

    async componentDidMount() {
        const automobiles = await fetch('http://localhost:8090/api/available-autos/');
        const salesPeople = await fetch('http://localhost:8090/api/sales-people/');
        const customers = await fetch('http://localhost:8090/api/customers/');
        if (automobiles.ok && salesPeople.ok && customers.ok) {
            const automobilesData = await automobiles.json();
            const salesPeopleData = await salesPeople.json();
            const customersData = await customers.json();
            this.setState({
                automobiles: automobilesData['available_autos'], 
                salesPeople: salesPeopleData.sales_people,
                customers: customersData.customers
            });
        }
    }

    render() {
        return(
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Record a new sale</h1>
                        <form onSubmit={this.handleSubmit}>
                            <div className="mb-3">
                                <select onChange={this.handleFieldChange} value={this.state.automobile} id="select-automobile" required name="automobile" className="form-select">
                                    <option value="default">Choose an automobile</option>
                                    {this.state.automobiles.map(auto => {
                                        return(
                                            <option key={auto.import_vin} value={auto.import_vin}>
                                                {auto.import_vin}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleFieldChange} value={this.state.salesPerson} id="select-sales-person" required name="salesPerson" className="form-select">
                                    <option value="default">Choose a sales person</option>
                                    {this.state.salesPeople.map(salesPerson => {
                                        return(
                                            <option key={salesPerson.employee_id} value={salesPerson.employee_id}>
                                                {salesPerson.name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleFieldChange} value={this.state.customer} id="select-customer" required name="customer" className="form-select">
                                    <option value="default">Choose a customer</option>
                                    {this.state.customers.map(customer => {
                                        return(
                                            <option key={customer.id} value={customer.id}>
                                                {customer.name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleFieldChange} value={this.state.price} placeholder="Price" required type="text" name="price" className="form-control"/>
                                <label htmlFor="price">Price</label>
                            </div>
                            <button className="btn btn-success">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}