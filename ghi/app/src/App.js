import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianForm from './ServiceJS/EnterTechnicianForm';
import SalesList from './SalesJS/SalesList';
import SelectSalesPerson from './SalesJS/SelectSalesPerson';
import RecordSaleForm from './SalesJS/RecordSaleForm';
import AppointmentForm from './ServiceJS/EnterAppointment';
import AppointmentList from './ServiceJS/ListOfAppointments';
import SalesPersonForm from './SalesJS/SalesPersonForm';
import CustomerForm from './SalesJS/CustomerForm';
import ManufacturerForm from './Inventory/AutoManufacturerForm';
import ManufacturersList from './Inventory/AutoManufacturerList';
import ModelForm from './Inventory/AutoModelForm';
import ModelsList from './Inventory/AutoModelList';
import AutoForm from './Inventory/AutoForm';
import AutosList from './Inventory/AutoList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          
          <Route path="sales/" >
            <Route index element={<SalesList />} />
            <Route path="new/" element={<RecordSaleForm />} />
            <Route path="sales-people/" element={<SelectSalesPerson />} />
            <Route path="sales-people/new/" element={<SalesPersonForm />} />
            <Route path="customers/new/" element={<CustomerForm />} />
          </Route>
          
          <Route path="technicians/new" element={<TechnicianForm />} />

          <Route path="service_appointment/">
            <Route path="new/" element={<AppointmentForm />} />
            <Route index element={<AppointmentList />} />
          </Route>

          <Route path="inventory" >
            <Route path="manufacturers/">
              <Route index element={<ManufacturersList />} />
              <Route path="new/" element={<ManufacturerForm />} />
            </Route>    
            <Route path="models/" >
              <Route index element={<ModelsList />} />
              <Route path="new/" element={<ModelForm />} />
            </Route>
            <Route path="automobiles">
              <Route index element={<AutosList />} />
              <Route path="new/" element={<AutoForm />} />
            </Route>

          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
