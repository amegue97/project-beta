import React, { useState, useEffect } from 'react';

export default function ModelsList() {
    const [models, setModels] = useState(null);

    useEffect(() => {
        async function getModels() {
            const response = await fetch('http://localhost:8100/api/models/');
            const data = await response.json();
            setModels(data.models);
        }

        if (!models) {
            getModels();
        }
    }, []);

    if (models === null) {
        return(
            <>Loading...</>
        )
    }

    return(
        <div>
            <h1>Models</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(model => {
                        return(
                            <tr key={model.id}>
                                <td>{model.name}</td>
                                <td>{model.manufacturer.name}</td>
                                <td>
                                    <img 
                                        src={model.picture_url}
                                        alt='loading failed'
                                        height='110px'
                                        width='150px'
                                    />
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}