import React, { useState, useEffect } from 'react';

export default function ManufacturersList() {
    const [manufacturers, setManufacturers] = useState(null);

    useEffect(() => {
        async function getManufacturers() {
            const response = await fetch('http://localhost:8100/api/manufacturers/');
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }

        if (!manufacturers) {
            getManufacturers();
        }
    }, []);

    if (manufacturers === null) {
        return(
            <>Loading...</>
        )
    }

    return(
        <div>
            <h1>Manufacturers</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Company Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map(manufacturer => {
                        return(
                            <tr key={manufacturer.id}>
                                <td>{manufacturer.name}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}