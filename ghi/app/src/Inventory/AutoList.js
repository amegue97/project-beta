import React, { useState, useEffect } from 'react';

export default function AutosList() {
    const [autos, setAutos] = useState(null);

    useEffect(() => {
        async function getAutos() {
            const response = await fetch('http://localhost:8100/api/automobiles/');
            const data = await response.json();
            setAutos(data.autos);
        }

        if (!autos) {
            getAutos();
        }
    }, []);

    if (autos === null) {
        return(
            <>Loading...</>
        )
    }

    return(
        <div>
            <h1>Automobiles</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                    </tr>
                </thead>
                <tbody>
                    {autos.map(auto => {
                        return(
                            <tr key={auto.id}>
                                <td>{auto.vin}</td>
                                <td>{auto.color}</td>
                                <td>{auto.year}</td>
                                <td>{auto.model.name}</td>
                                <td>{auto.model.manufacturer.name}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}