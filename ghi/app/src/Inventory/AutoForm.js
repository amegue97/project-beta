import React from "react";

export default class AutoForm extends React.Component {
    constructor() {
        super();
        this.state = {
            color: '',
            year: '',
            vin: '',
            model: '',
            models: []
        }
        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();

        const data = {...this.state};
        data["model_id"] = data.model;
        delete data.model;
        delete data.models;

        const autosUrl = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(autosUrl, fetchConfig);
        if (response.ok) {
            const clearForm = {
                color: '',
                year: '',
                vin: '',
                model: ''
            };
            this.setState(clearForm);
        }
    }

    handleFieldChange(event) {
        this.setState({
            ...this.state,
            [event.target.name]: event.target.value
        })
    }

    async componentDidMount() {
        const response = await fetch('http://localhost:8100/api/models/');
        
        if (response.ok) {
            const data = await response.json();
            this.setState({models: data.models})
        }
    }

    render() {
        return(
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a Car</h1>
                        <form onSubmit={this.handleSubmit}>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleFieldChange} value={this.state.color} placeholder="Color" required type="text" name="color" className="form-control" />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleFieldChange} value={this.state.year} placeholder="Year" required type="text" name="year" className="form-control" />
                                <label htmlFor="year">Year</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleFieldChange} value={this.state.vin} placeholder="VIN" required type="text" name="vin" className="form-control" />
                                <label htmlFor="vin">VIN</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleFieldChange} value={this.state.model} id="select-model" required name="model" className="form-select">
                                    <option value="default">Choose a model</option>
                                    {this.state.models.map(model => {
                                        return(
                                            <option key={model.href} value={model.id}>
                                                {model.manufacturer.name} {model.name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-success">Create</button>
                        </form>
                    </div>    
                </div>
            </div>
        )
    }
}