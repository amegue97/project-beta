import React from "react";

export default class ManufacturerForm extends React.Component {
    constructor() {
        super();
        this.state = {
            name: ''
        }
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state}

        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            header: {
                'Content-Type': 'application/json'
            },
        }

        const response = await fetch(manufacturerUrl, fetchConfig);
        if (response.ok) {
            this.setState({name: ''})
        }
    }

    handleNameChange(event) {
        this.setState(
            {name: event.target.value}
        )
    }

    render() {
        return(
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a Manufacturer</h1>
                        <form onSubmit={this.handleSubmit}>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name" className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <button className="btn btn-success">Create</button>
                        </form>
                    </div>    
                </div>
            </div>
        )
    }
}