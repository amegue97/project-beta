import React from "react";

export default class ModelForm extends React.Component {
    constructor() {
        super();
        this.state = {
            name: '',
            pictureUrl: '',
            manufacturer: '',
            manufacturers: []
        }
        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();

        const data = {};
        data["name"] = this.state.name;
        data["picture_url"] = this.state.pictureUrl;
        data["manufacturer_id"] = this.state.manufacturer;
        console.log(data)

        const modelsUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(modelsUrl, fetchConfig);
        if (response.ok) {
            const clearForm = {
                name: '',
                pictureUrl: '',
                manufacturer: ''
            };
            this.setState(clearForm);
        }
    }

    handleFieldChange(event) {
        this.setState({
            ...this.state,
            [event.target.name]: event.target.value
        })
    }

    async componentDidMount() {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        
        if (response.ok) {
            const data = await response.json();
            this.setState({manufacturers: data.manufacturers})
        }
    }

    render() {
        return(
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a Model</h1>
                        <form onSubmit={this.handleSubmit}>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleFieldChange} value={this.state.name} placeholder="Name" required type="text" name="name" className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleFieldChange} value={this.state.pictureUrl} placeholder="Picture URL" required type="text" name="pictureUrl" className="form-control" />
                                <label htmlFor="picture-url">Picture</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleFieldChange} value={this.state.manufacturer} id="select-manufacturer" required name="manufacturer" className="form-select">
                                    <option value="default">Choose a manufacturer</option>
                                    {this.state.manufacturers.map(manufacturer => {
                                        return(
                                            <option key={manufacturer.id} value={manufacturer.id}>
                                                {manufacturer.name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-success">Create</button>
                        </form>
                    </div>    
                </div>
            </div>
        )
    }
}